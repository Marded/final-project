# "Závěrečná" práce

### **Popis**

Tato webová aplikace slouží k evidenci servisních úkonů a prohlídek jednotlivých aut. Aplikace umožnuje vytvořit několik rolí. Pokud se jedná o *automechanika*, tak ten má jiné práva než např. *administrátor* nebo *zákazník*. Aplikace ulehčuje přehled prováděných zásahů a mít možnost zpětně dohledat dřívější opravy.

### **Instalace a použití**

K instalaci aplikace je potřeba:

1. Instalace knihovny jazzmin
  * Použite: `pip install -U django-jazzmin`
1. Otevřete projekt např. PyCharm
1. V terminálu spustit
  * `python ./manage.py runserver` nebo `python manage.py runserver 0.0.0.0:8000`
  * Druhý příkaz slouží pro spuštění např. pomocí veřejné IP adresy *např. 192.168.45.100:8000*
  * Default je localhost
1. Následně bychom měli vidět úvodní stránku aplikace
1. Admin rozhraní zobrazíme
  * `vaše adresa/admin`

### **Prekvizity**

Před spuštěním aplikace je potřebá nainstalovat následující

* Python (verze 3.10) [Download](https://www.python.org/downloads/)
* Django (verze 5.0.0) [Download](https://www.djangoproject.com/download/)
* Jazzmin [Install](https://django-jazzmin.readthedocs.io/installation/)

### **Licence**

Tento projekt jsem vytvořil v rámci projektu v předmětu SKJ. **Autorem je @marded [DED0070]**

### **Požadavky práce**

V rámci práce by mělo být splněno:

* [ ] GIT repozitář na Gitlabu.
  * [ ] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [ ] Použití vlastního projektu (ideální kandidát má kombinaci jazyků - např. Python a BASH, Markdown a JS, apod.)
* [ ] Vytvořená CI v repozitáři.
* [ ] CI má minimálně tři úlohy:
  * [ ] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá. Minimálně README.md by mělo být v repozitáři dostupné.)
  * [ ] Kontrola syntaxe každého jazyka.
  * [ ] Použití "lint" nástroje pro každý jazyk.
* [ ] (Volitelné/doporučené) Nástroj na kontrolu dostupnosti aktualizací/security updates pro moduly jazyka (pokud existuje).
* [ ] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage (pokud existuje).
* [ ] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML reporty - umistěte je na Gitlab pages.
* [ ] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.

### **Ukázka webu**

![Ukázka aplikace!](images/index.png "Ukázka aplikace")
