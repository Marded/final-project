* [ ] GIT repozitář na Gitlabu.
  * [ ] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [ ] Použití vlastního projektu (ideální kandidát má kombinaci jazyků - např. Python a BASH, Markdown a JS, apod.)
* [ ] Vytvořená CI v repozitáři.
* [ ] CI má minimálně tři úlohy:
  * [ ] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá. Minimálně README.md by mělo být v repozitáři dostupné.)
  * [ ] Kontrola syntaxe každého jazyka.
  * [ ] Použití "lint" nástroje pro každý jazyk.
* [ ] (Volitelné/doporučené) Nástroj na kontrolu dostupnosti aktualizací/security updates pro moduly jazyka (pokud existuje).
* [ ] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage (pokud existuje).
* [ ] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML reporty - umistěte je na Gitlab pages.
* [ ] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.
