"""
URL configuration for Autoservis project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path

from Autoservis import url_handlers
from AutoservisApp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='base'),
    path('kontakt/', views.kontakt, name='kontakt'),
    path('zakazky/', views.zakazky, name='zakazky'),
    path('zakazkaAdd/', views.zakazkaAdd, name='zakazkaAdd'),
    path('majitele/', views.majitele, name='majitele'),
    path('majitelAdd/', views.majitelAdd, name='majitelAdd'),
    path('vozidla/', views.vozidla, name='vozidla'),
    path('vozidloAdd/', views.vozidloAdd, name='vozidloAdd'),
    path('historie_oprav/', views.historie_oprav, name='historie_oprav'),
    path('historieAdd/', views.historieAdd, name='historieAdd'),
    path('pracovni_ukon/', views.pracovni_ukon, name='pracovni_ukon'),
    path('ukonAdd/', views.ukonAdd, name='ukonAdd'),
    path('sklad_ND/', views.sklad_ND, name='sklad_ND'),
    path('skladAdd/', views.skladAdd, name='skladAdd'),
    path('skladDelete/<int:sklad_id>/', views.skladDelete, name='skladDelete'),
    path('zakazkaDelete/<int:zakazka_id>/', views.zakazkaDelete, name='zakazkaDelete'),
    path('majitelDelete/<int:majitel_id>/', views.majitelDelete, name='majitelDelete'),
    path('vozidloDelete/<int:vozidlo_id>/', views.vozidloDelete, name='vozidloDelete'),
    path('historieDelete/<int:historie_id>/', views.historieDelete, name='historieDelete'),
    path('ukonDelete/<int:ukon_id>/', views.ukonDelete, name='ukonDelete'),
    path('skladUpdate/<int:sklad_id>/', views.skladUpdate, name='skladUpdate'),
    path('ukonUpdate/<int:ukon_id>/', views.ukonUpdate, name='ukonUpdate'),
    path('historieUpdate/<int:historie_id>/', views.historieUpdate, name='historieUpdate'),
    path('vozidloUpdate/<int:vozidlo_id>/', views.vozidloUpdate, name='vozidloUpdate'),
    path('majitelUpdate/<int:majitel_id>/', views.majitelUpdate, name='majitelUpdate'),
    path('zakazkaUpdate/<int:zakazka_id>/', views.zakazkaUpdate, name='zakazkaUpdate'),
    path('skladDetail/<int:sklad_id>/', views.skladDetail, name='skladDetail'),
    path('ukonDetail/<int:ukon_id>/', views.ukonDetail, name='ukonDetail'),
    path('historieDetail/<int:historie_id>/', views.historieDetail, name='historieDetail'),
    path('vozidloDetail/<int:vozidlo_id>/', views.vozidloDetail, name='vozidloDetail'),
    path('majitelDetail/<int:majitel_id>/', views.majitelDetail, name='majitelDetail'),
    path('zakazkaDetail/<int:zakazka_id>/', views.zakazkaDetail, name='zakazkaDetail'),
    path("register/", views.UzivatelViewRegister.as_view(), name="registrace"),
    path("login/", views.UzivatelViewLogin.as_view(), name="login"),
    path("logout/", views.logout_user, name="logout"),
    path("", url_handlers.index_handler),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
