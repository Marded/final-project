from django.contrib import admin
from django.db import models
from django import forms
from .models import Zakazky, Majitel, Vozidlo, PracovniUkon, HistorieOprav, SkladND, Uzivatel, UzivatelManager
from .widgets import PastCustomDatePickerWidget, PastCustomTimePickerWidget
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class ZakazkyAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.DateField: {'widget': PastCustomDatePickerWidget},
        models.TimeField: {'widget': PastCustomTimePickerWidget},
    }
    list_display = ['Nazev', 'Popis_zavady', 'Datum_prijmu', 'Cas_prijmu']
    list_filter = ['Nazev', 'Datum_prijmu']
    list_display_links = ['Nazev']
    search_fields = ['Nazev__startswith']
    list_per_page = 20

class MajitelAdmin(admin.ModelAdmin):
    list_display = ['Jmeno', 'Prijmeni', 'Ulice', 'Mesto', 'PSC', 'Email', 'Mobil']
    list_filter = ['Mesto', 'PSC']
    search_fields = ['Prijmeni__startswith']
    list_display_links = ['Prijmeni', 'Email']
    list_per_page = 20

class VozidloAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.DateField: {'widget': PastCustomDatePickerWidget},
    }
    list_display = ['Znacka', 'Typ', 'Motor', 'SPZ', 'VIN', 'Foto']
    list_filter = ['Znacka', 'Typ', 'Motor']
    search_fields = ['SPZ__startswith', 'VIN__startswith']
    list_display_links = ['SPZ', 'VIN']
    list_per_page = 20

class PracovniUkonAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.DateField: {'widget': PastCustomDatePickerWidget},
    }
    list_display = ['Pozice', 'Popis', 'Cena', 'Datum_provedeni']
    search_fields = ['Popis__startswith']
    list_filter = ['Pozice', 'Datum_provedeni']
    list_per_page = 20

class HistorieOpravAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.DateField: {'widget': PastCustomDatePickerWidget},
    }
    list_display = ['Celkova_cena', 'Dokonceno', 'PracovniUkon_Id_ukonu']
    list_display_links = ['PracovniUkon_Id_ukonu']
    list_filter = ['PracovniUkon_Id_ukonu']
    list_per_page = 20

class SkladNDAdmin(admin.ModelAdmin):
    list_display = ['Nazev', 'Vyrobce', 'Cena_ND', 'Pocet_kusu']
    list_filter = ['Vyrobce']
    search_fields = ['Nazev__startswith']
    list_display_links = ['Nazev']
    list_per_page = 20


class UzivatelCreationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = Uzivatel
        fields = ['email']

    def save(self, commit=True):
        if self.is_valid():
            user = super().save(commit=False)
            user.set_password(self.cleaned_data["password"])
            if commit:
                user.save()
            return user


class UzivatelChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Uzivatel
        fields = ['email', 'is_admin']

    def __init__(self, *args, **kwargs):
        super(UzivatelChangeForm, self).__init__(*args, **kwargs)
        self.Meta.fields.remove('password')


class UzivatelAdmin(UserAdmin):
    form = UzivatelChangeForm
    add_form = UzivatelCreationForm

    list_display = ['email', 'is_admin']
    list_filter = ['is_admin']
    fieldsets = (
        (None, {'fields': ['email', 'password']}),
        ('Permissions', {'fields': ['is_admin']}),
    )

    add_fieldsets = (
        (None, {
            'fields': ['email', 'password']}
         ),
    )
    search_fields = ['email']
    ordering = ['email']
    filter_horizontal = []

admin.site.register(Zakazky, ZakazkyAdmin)
admin.site.register(Majitel, MajitelAdmin)
admin.site.register(Vozidlo, VozidloAdmin)
admin.site.register(PracovniUkon, PracovniUkonAdmin)
admin.site.register(HistorieOprav, HistorieOpravAdmin)
admin.site.register(SkladND, SkladNDAdmin)
admin.site.register(Uzivatel, UzivatelAdmin)