from datetime import date, datetime
from django import forms

class CustomDatePickerWidget(forms.DateInput):
    date_input_format = '%Y-%m-%d'

    def __init__(self, attrs = {}, format = None):
        attrs.update(
            {
                'class': 'form-control',
                'type': 'date',
            }
        )
        self.format = format or self.date_input_format
        super().__init__(attrs, format = self.format)

class PastCustomDatePickerWidget(CustomDatePickerWidget):
    def __init__(self, attrs = {}, format = None):
        attrs.update({'max': date.today()})
        super().__init__(attrs, format = format)

class CustomTimePickerWidget(forms.TimeInput):
    time_input_format = '%H:%M'

    def __init__(self, attrs = {}, format = None):
        attrs.update(
            {
                'class': 'form-control',
                'type': 'time',
            }
        )
        self.format = format or self.time_input_format
        super().__init__(attrs, format = self.format)

class PastCustomTimePickerWidget(CustomTimePickerWidget):
    def __init__(self, attrs = {}, format = None):
        attrs.update({'max': datetime.now().strftime('%H:%M')})
        super().__init__(attrs, format = format)