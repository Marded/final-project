import os

from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.views import generic
from django.contrib import messages

from .forms import MajitelForm, ZakazkaForm, HistorieForm, UkonForm, SkladForm, VozidloForm, LoginForm, UzivatelForm
from .models import Zakazky, Majitel, Vozidlo, HistorieOprav, PracovniUkon, SkladND, Uzivatel
from django.contrib.auth import login, logout, authenticate

# Create your views here.

def index(request):
    return render(request, 'base.html')

def kontakt(request):
    return render(request, 'Autoservis/kontakt.html')

def zakazky(request):
    zakazky = Zakazky.objects.all()
    return render(request, 'Autoservis/zakazky.html', {'zakazky': zakazky})

def zakazkaAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("zakazky"))
    if request.method == 'POST':
        zakazka_form = ZakazkaForm(request.POST)
        if zakazka_form.is_valid():
            zakazka_form.save()
            messages.success(request, "Vložení proběhlo úspěšně")
            return redirect('zakazky')
    else:
        zakazka_form = ZakazkaForm

    return render(request, 'Autoservis/zakazkaAdd.html', {'zakazka_form': zakazka_form})

def pracovni_ukon_detail(request, pracovni_ukon_id):
    pracovni_ukon = PracovniUkon.objects.get(pk = pracovni_ukon_id)
    return render(request, 'Autoservis/pracovni_ukon.html', {'pracovni_ukon': pracovni_ukon})

def majitele(request):
    majitele = Majitel.objects.all()
    majitel_form = MajitelForm
    return render(request, 'Autoservis/majitele.html', {'majitele': majitele, 'majitel_form': majitel_form})

def majitelAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("majitele"))
    if request.method == 'POST':
        majitel_form = MajitelForm(request.POST)
        if majitel_form.is_valid():
            majitel_form.save()
            messages.success(request, "Vložení proběhlo úspěšně")
            return redirect('majitele')
    else:
        majitel_form = MajitelForm()

    return render(request, 'Autoservis/majitelAdd.html', {'majitel_form': majitel_form, 'submitted': submitted})

def vozidla(request):
    vozidla = Vozidlo.objects.all()
    return render(request, 'Autoservis/vozidla.html', {'vozidla': vozidla})

def vozidloAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("vozidla"))
    if request.method == 'POST':
        vozidlo_form = VozidloForm(request.POST, request.FILES)
        if vozidlo_form.is_valid():
            vozidlo_form.save()
            messages.success(request, "Vložení proběhlo úspěšně")
            return redirect('vozidla')
    else:
        vozidlo_form = VozidloForm

    return render(request, 'Autoservis/vozidloAdd.html', {'vozidlo_form': vozidlo_form})

def historie_oprav(request):
    historie_oprav = HistorieOprav.objects.all()
    return render(request, 'Autoservis/historie_oprav.html', {'historie_oprav': historie_oprav})

def historieAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("historie_oprav"))
    if request.method == 'POST':
        historie_form = HistorieForm(request.POST)
        if historie_form.is_valid():
            historie_form.save()
            messages.success(request, "Vložení proběhlo úspěšně")
            return redirect('historie_oprav')
    else:
        historie_form = HistorieForm

    return render(request, 'Autoservis/historieAdd.html', {'historie_form': historie_form})

def pracovni_ukon(request):
    pracovni_ukon = PracovniUkon.objects.all()
    return render(request, 'Autoservis/pracovni_ukon.html', {'pracovni_ukon': pracovni_ukon})

def ukonAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("pracovni_ukon"))
    if request.method == 'POST':
        ukon_form = UkonForm(request.POST)
        if ukon_form.is_valid():
            ukon_form.save()
            messages.success(request, "Vložení proběhlo úspěšně")
            return redirect('pracovni_ukon')
    else:
        ukon_form = UkonForm

    return render(request, 'Autoservis/ukonAdd.html', {'ukon_form': ukon_form})

def sklad_ND(request):
    sklad_ND = SkladND.objects.all()
    return render(request, 'Autoservis/sklad_ND.html', {'sklad_ND': sklad_ND})

def skladAdd(request):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro přidání")
        return redirect(reverse("sklad_ND"))
    if request.method == 'POST':
        sklad_form = SkladForm(request.POST)
        if sklad_form.is_valid():
            sklad_form.save()
            return redirect('sklad_ND')
    else:
        sklad_form = SkladForm

    return render(request, 'Autoservis/skladAdd.html', {'sklad_form': sklad_form})

def skladDelete(request, sklad_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("sklad_ND"))
    sklad = get_object_or_404(SkladND, pk=sklad_id)
    sklad.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('sklad_ND')

def zakazkaDelete(request, zakazka_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("zakazky"))
    zakazka = get_object_or_404(Zakazky, pk=zakazka_id)
    zakazka.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('zakazky')

def majitelDelete(request, majitel_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("majitele"))
    majitel = get_object_or_404(Majitel, pk=majitel_id)
    majitel.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('majitele')

def vozidloDelete(request, vozidlo_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("vozidla"))
    vozidlo = get_object_or_404(Vozidlo, pk=vozidlo_id)
    if bool(vozidlo.Foto):
        if len(vozidlo.Foto) > 0:
            os.remove(vozidlo.Foto.path)
        vozidlo.delete()
    else:
        vozidlo.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('vozidla')

def historieDelete(request, historie_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("historie_oprav"))
    historie = get_object_or_404(HistorieOprav, pk=historie_id)
    historie.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('historie_oprav')

def ukonDelete(request, ukon_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro odstranění")
        return redirect(reverse("pracovni_ukon"))
    ukon = get_object_or_404(PracovniUkon, pk=ukon_id)
    ukon.delete()
    messages.success(request, "Odstranění proběhlo úspěšně")
    return redirect('pracovni_ukon')

def skladUpdate(request, sklad_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("sklad_ND"))
    sklad = get_object_or_404(SkladND, pk=sklad_id)
    sklad_form = SkladForm(request.POST or None, instance=sklad)

    if sklad_form.is_valid():
        sklad_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('sklad_ND')

    return render(request, 'Autoservis/skladUpdate.html', {'sklad': sklad, 'sklad_form': sklad_form})

def ukonUpdate(request, ukon_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("pracovni_ukon"))
    ukon = get_object_or_404(PracovniUkon, pk=ukon_id)
    ukon_form = UkonForm(request.POST or None, instance=ukon)

    if ukon_form.is_valid():
        ukon_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('pracovni_ukon')

    return render(request, 'Autoservis/ukonUpdate.html', {'ukon': ukon, 'ukon_form': ukon_form})

def historieUpdate(request, historie_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("historie_oprav"))
    historie = get_object_or_404(HistorieOprav, pk=historie_id)
    historie_form = HistorieForm(request.POST or None, instance=historie)

    if historie_form.is_valid():
        historie_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('historie_oprav')

    return render(request, 'Autoservis/historieUpdate.html', {'historie': historie, 'historie_form': historie_form})

def vozidloUpdate(request, vozidlo_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("vozidla"))
    vozidlo = get_object_or_404(Vozidlo, pk=vozidlo_id)
    vozidlo_form = VozidloForm(request.POST or None, request.FILES or None, instance=vozidlo)

    if vozidlo_form.is_valid():
        vozidlo_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('vozidla')

    return render(request, 'Autoservis/vozidloUpdate.html', {'vozidlo': vozidlo, 'vozidlo_form': vozidlo_form})

def majitelUpdate(request, majitel_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("majitele"))
    majitel = get_object_or_404(Majitel, pk=majitel_id)
    majitel_form = MajitelForm(request.POST or None, instance=majitel)

    if majitel_form.is_valid():
        majitel_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('majitele')

    return render(request, 'Autoservis/majitelUpdate.html', {'majitel': majitel, 'majitel_form': majitel_form})

def zakazkaUpdate(request, zakazka_id):
    if not request.user.is_admin:
        messages.warning(request, "Nemáte práva pro aktualizaci")
        return redirect(reverse("zakazky"))
    zakazka = get_object_or_404(Zakazky, pk=zakazka_id)
    zakazka_form = ZakazkaForm(request.POST or None, instance=zakazka)

    if zakazka_form.is_valid():
        zakazka_form.save()
        messages.success(request, "Aktualizace proběhla úspěšně")
        return redirect('zakazky')

    return render(request, 'Autoservis/zakazkaUpdate.html', {'zakazka': zakazka, 'zakazka_form': zakazka_form})

def skladDetail(request, sklad_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("sklad_ND"))
    sklad = get_object_or_404(SkladND, pk=sklad_id)

    return render(request, 'Autoservis/skladDetail.html', {'sklad': sklad})

def ukonDetail(request, ukon_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("pracovni_ukon"))
    ukon = get_object_or_404(PracovniUkon, pk=ukon_id)

    return render(request, 'Autoservis/ukonDetail.html', {'ukon': ukon})

def historieDetail(request, historie_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("historie_oprav"))
    historie = get_object_or_404(HistorieOprav, pk=historie_id)

    return render(request, 'Autoservis/historieDetail.html', {'historie': historie})

def vozidloDetail(request, vozidlo_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("vozidla"))
    vozidlo = get_object_or_404(Vozidlo, pk=vozidlo_id)
    #images = Vozidlo.objects.filter(Foto=vozidlo)

    return render(request, 'Autoservis/vozidloDetail.html', {'vozidlo': vozidlo})

def majitelDetail(request, majitel_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("majitele"))
    majitel = get_object_or_404(Majitel, pk=majitel_id)

    return render(request, 'Autoservis/majitelDetail.html', {'majitel': majitel})

def zakazkaDetail(request, zakazka_id):
    if not request.user.is_authenticated:
        messages.warning(request, "Pro detail se musíte přihlásit")
        return redirect(reverse("zakazky"))
    zakazka = get_object_or_404(Zakazky, pk=zakazka_id)

    return render(request, 'Autoservis/zakazkaDetail.html', {'zakazka': zakazka})


class UzivatelViewRegister(generic.edit.CreateView):
    form_class = UzivatelForm
    model = Uzivatel
    template_name = "Autoservis/user_form.html"

    def get(self, request):
        if request.user.is_authenticated:
            messages.info(request, "Už jsi přihlášený, nemůžeš se registrovat.")
            return redirect(reverse("base"))
        else:
            form = self.form_class(None)
        return render(request, self.template_name, {"form": form})

    def post(self, request):
        if request.user.is_authenticated:
            messages.info(request, "Už jsi přihlášený, nemůžeš se registrovat.")
            return redirect(reverse("base"))
        form = self.form_class(request.POST)
        if form.is_valid():
            uzivatel = form.save(commit=False)
            password = form.cleaned_data["password"]
            uzivatel.set_password(password)
            uzivatel.save()
            login(request, uzivatel)
            return redirect("base")

        return render(request, self.template_name, {"form": form})

class UzivatelViewLogin(generic.edit.CreateView):
    form_class = LoginForm
    template_name = "Autoservis/user_form.html"

    def get(self, request):
        if request.user.is_authenticated:
            messages.warning(request, "Již jste přihlášení")
            return redirect(reverse("base"))
        else:
            form = self.form_class(None)
        return render(request, self.template_name, {"form": form})

    def post(self, request):
        if request.user.is_authenticated:
            messages.warning(request, "Již jste přihlášení")
            return redirect(reverse("base"))
        form = self.form_class(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            user = authenticate(email=email, password=password)
            if user:
                login(request, user)
                return redirect("base")
            else:
                messages.error(request, "Tento účet neexistuje")
        return render(request, self.template_name, {"form": form})

def logout_user(request):
    if request.user.is_authenticated:
        logout(request)
    else:
        messages.warning(request, "Nemůžete se odhlásit, když nejste přihlášení")
    return redirect(reverse("base"))