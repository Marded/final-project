from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models

class SkladND(models.Model):
    Id_ND = models.AutoField(primary_key = True)
    Nazev = models.CharField('Název', max_length = 50)
    Vyrobce = models.CharField('Výrobce', max_length = 50)
    Cena_ND = models.PositiveIntegerField('Cena ND')
    Pocet_kusu = models.PositiveSmallIntegerField('Počet kusů')

    def __str__(self):
        return f'{self.Nazev} - {self.Vyrobce}'

    class Meta:
        verbose_name = 'Sklad ND'
        verbose_name_plural = 'Sklad ND'
        db_table = 'Sklad_ND'

class PracovniUkon(models.Model):
    Id_ukonu = models.AutoField(primary_key = True)
    Pozice = models.CharField(max_length = 20)
    Popis = models.CharField(max_length = 150)
    Cena = models.PositiveIntegerField()
    Datum_provedeni = models.DateField('Datum provedení')
    SkladND_Id_ND = models.ManyToManyField(verbose_name = 'ND', to = 'SkladND')

    def __str__(self):
        return f'{self.Pozice} - {self.Popis}'

    class Meta:
        verbose_name = 'Pracovní úkon'
        verbose_name_plural = 'Pracovní úkony'
        db_table = 'Pracovni_ukon'

class HistorieOprav(models.Model):
    Id_opravy = models.AutoField(primary_key = True)
    Celkova_cena = models.PositiveIntegerField('Celková cena')
    Dokonceno = models.DateField('Dokončeno')
    PracovniUkon_Id_ukonu = models.ForeignKey(PracovniUkon, on_delete = models.PROTECT, verbose_name = 'Úkon')

    def __str__(self):
        return f'{self.PracovniUkon_Id_ukonu.Popis} - Celková cena: {self.Celkova_cena}'

    class Meta:
        verbose_name = 'Historie oprav'
        verbose_name_plural = 'Historie oprav'
        db_table = 'Historie_oprav'

class Vozidlo(models.Model):
    Id_vozidla = models.AutoField(primary_key = True)
    Znacka = models.CharField('Značka', max_length = 30)
    Typ = models.CharField(max_length = 30)
    Motor = models.CharField(max_length = 20)
    SPZ = models.CharField(max_length = 7)
    VIN = models.CharField(max_length = 30)
    Tachometr = models.PositiveIntegerField()
    V_provozu_od = models.DateField('V provozu od', null = True, blank = True)
    Foto = models.ImageField(upload_to = 'vozidla/', null = True, blank = True)
    HistorieOprav_Id_opravy = models.ManyToManyField(verbose_name = 'Oprava', to = 'HistorieOprav')

    def __str__(self):
        return f'{self.Znacka} {self.Typ} - {self.SPZ}'

    class Meta:
        verbose_name = 'Vozidlo'
        verbose_name_plural = 'Vozidla'
        db_table = 'Vozidlo'

class Zakazky(models.Model):
    Id_zakazky = models.AutoField(primary_key = True)
    Nazev = models.CharField('Název', max_length = 50)
    Popis_zavady = models.TextField('Popis závady', max_length = 150, null = True, blank = True)
    Datum_prijmu = models.DateField('Datum příjmu')
    Cas_prijmu = models.TimeField('Čas příjmu')
    PracovniUkon_Id_ukonu = models.ManyToManyField(verbose_name = 'Pracovní úkony', to = 'PracovniUkon')
    Razitko_zakazky = models.DateTimeField(auto_now_add = True)

    class Meta:
        verbose_name = 'Zákazka'
        verbose_name_plural = 'Zákazky'
        db_table = 'Zakazka'

    def __str__(self):
        return f'{self.Nazev} - {str(self.Datum_prijmu.strftime(("%d.%m.%Y")))}, {str(self.Cas_prijmu.strftime(("%H:%M")))}'

class Majitel(models.Model):
    Id_majitele = models.AutoField(primary_key = True)
    Jmeno = models.CharField('Jméno', max_length = 50)
    Prijmeni = models.CharField('Příjmení', max_length = 50)
    Ulice = models.CharField(max_length = 100)
    Mesto = models.CharField('Město', max_length = 100)
    PSC = models.CharField('PSČ', max_length = 5)
    Email = models.EmailField(max_length = 100)
    Mobil = models.CharField(max_length = 13)
    Vozidlo_Id_vozidla = models.ManyToManyField(verbose_name = 'Vozidla', to = 'Vozidlo')
    Zakazky_Id_zakazky = models.ManyToManyField(verbose_name = 'Zákazky', to = 'Zakazky')

    def __str__(self):
        return f'{self.Jmeno} {self.Prijmeni}'

    class Meta:
        verbose_name = 'Majitel'
        verbose_name_plural = 'Majitelé'
        db_table = 'Majitel'

class UzivatelManager(BaseUserManager):
    def create_user(self, email, password):
        print(self.model)
        if email and password:
            user = self.model(email=self.normalize_email(email))
            user.set_password(password)
            user.save()
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_admin = True
        user.save()
        return user

class Uzivatel(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=300, unique=True)
    is_admin = models.BooleanField('Admin', default=False)

    class Meta:
        verbose_name = "uživatel"
        verbose_name_plural = "uživatelé"

    objects = UzivatelManager()

    USERNAME_FIELD = "email"

    def __str__(self):
        return "email: {}".format(self.email)

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True