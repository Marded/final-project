# Generated by Django 4.2 on 2023-04-10 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AutoservisApp', '0009_remove_zakazky_vozidlo_id_vozidla'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zakazky',
            name='PracovniUkon_Id_ukonu',
        ),
        migrations.AddField(
            model_name='zakazky',
            name='PracovniUkon_Id_ukonu',
            field=models.ManyToManyField(to='AutoservisApp.pracovniukon'),
        ),
    ]
