from django.apps import AppConfig


class AutoservisappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AutoservisApp'
