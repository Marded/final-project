from django import forms
from .models import Majitel, Zakazky, HistorieOprav, PracovniUkon, SkladND, Vozidlo, Uzivatel


class MajitelForm(forms.ModelForm):
    class Meta:
        model = Majitel
        fields = ('Jmeno', 'Prijmeni', 'Ulice', 'Mesto', 'PSC', 'Email', 'Mobil', 'Vozidlo_Id_vozidla', 'Zakazky_Id_zakazky')
        labels = {
            'Jmeno': '',
            'Prijmeni': '',
            'Ulice': '',
            'Mesto': '',
            'PSC': '',
            'Email': '',
            'Mobil': ''
        }
        widgets = {
            'Jmeno': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Jméno'}),
            'Prijmeni': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Příjmení'}),
            'Ulice': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ulice'}),
            'Mesto': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Město'}),
            'PSC': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'PSČ'}),
            'Email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}),
            'Mobil': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Mobil'}),
            'Vozidlo_Id_vozidla': forms.SelectMultiple(attrs={'class': 'form-select'}),
            'Zakazky_Id_zakazky': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

class ZakazkaForm(forms.ModelForm):
    class Meta:
        model = Zakazky
        fields = ('Nazev', 'Popis_zavady', 'Datum_prijmu', 'Cas_prijmu', 'PracovniUkon_Id_ukonu')
        labels = {
            'Nazev': '',
            'Popis_zavady': '',
            'Datum_prijmu': '',
            'Cas_prijmu': '',
        }
        widgets = {
            'Nazev': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Název'}),
            'Popis_zavady': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Popis závady'}),
            'Datum_prijmu': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Datum příjmu', 'type': 'date'}),
            'Cas_prijmu': forms.TimeInput(attrs={'class': 'form-control', 'placeholder': 'Čas příjmu', 'type': 'time'}),
            'PracovniUkon_Id_ukonu': forms.SelectMultiple(attrs={'class': 'form-select'}),
        }

class HistorieForm(forms.ModelForm):
    class Meta:
        model = HistorieOprav
        fields = ('Celkova_cena', 'Dokonceno', 'PracovniUkon_Id_ukonu')
        labels = {
            'Celkova_cena': '',
            'Dokonceno': '',
        }
        widgets = {
            'Celkova_cena': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Celková cena'}),
            'Dokonceno': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Dokončeno', 'type': 'date'}),
            'PracovniUkon_Id_ukonu': forms.Select(attrs={'class': 'form-control'}),
        }

class UkonForm(forms.ModelForm):
    class Meta:
        model = PracovniUkon
        fields = ('Pozice', 'Popis', 'Cena', 'Datum_provedeni', 'SkladND_Id_ND')
        labels = {
            'Pozice': '',
            'Popis': '',
            'Cena': '',
            'Datum_provedeni': '',
        }
        widgets = {
            'Pozice': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Pozice'}),
            'Popis': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Popis'}),
            'Cena': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Cena'}),
            'Datum_provedeni': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Datum provedení', 'type': 'date'}),
            'SkladND_Id_ND': forms.SelectMultiple(attrs={'class': 'form-control'}),
        }

class SkladForm(forms.ModelForm):
    class Meta:
        model = SkladND
        fields = ('Nazev', 'Vyrobce', 'Cena_ND', 'Pocet_kusu')
        labels = {
            'Nazev': '',
            'Vyrobce': '',
            'Cena_ND': '',
            'Pocet_kusu': '',
        }
        widgets = {
            'Nazev': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Název'}),
            'Vyrobce': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Výrobce'}),
            'Cena_ND': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Cena ND'}),
            'Pocet_kusu': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Počet kusů'}),
        }

class VozidloForm(forms.ModelForm):
    class Meta:
        model = Vozidlo
        fields = ('Znacka', 'Typ', 'Motor', 'SPZ', 'VIN', 'Tachometr', 'V_provozu_od', 'Foto', 'HistorieOprav_Id_opravy')
        labels = {
            'Znacka': '',
            'Typ': '',
            'Motor': '',
            'SPZ': '',
            'VIN': '',
            'Tachometr': '',
            'V_provozu_od': '',
        }
        widgets = {
            'Znacka': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Značka'}),
            'Typ': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Typ'}),
            'Motor': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Motor'}),
            'SPZ': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'SPZ'}),
            'VIN': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'VIN'}),
            'Tachometr': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Tachometr'}),
            'V_provozu_od': forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'Foto': forms.FileInput(attrs={'class': 'form-control', 'accept': 'image/jpeg, image/png, image/jpg'}),
            'HistorieOprav_Id_opravy': forms.SelectMultiple(attrs={'class': 'form-control'}),
        }

class UzivatelForm(forms.ModelForm):
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Heslo'}))

    class Meta:
        model = Uzivatel
        fields = ("email", "password")
        labels = {
            'email': ''
        }
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'})
        }

class LoginForm(forms.Form):
    email = forms.CharField(label='', widget=forms.EmailInput(attrs={'placeholder': 'Email'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Heslo'}))